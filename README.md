<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

TODO: Put a short description of the package here that helps potential users
know whether this package might be useful for them.


### Rework
* Barcode scanner dialog
* Barcode scanner widget
* Barcode provider (keyboard, usb(vendorId, productId), Bluetooth?)
* Removie lottie and install rive

## Features

TODO: List what your package can do. Maybe include images, gifs, or videos.

## Getting started

TODO: List prerequisites and provide or point to information on how to
start using the package.

## Usage

```dart
BarcodeScannerReader? reader = BarcodeScannerReader.create(['NT1640S']);

if (reader != null) {

    reader.stream.listen((data) {
        log(String.fromCharCodes(data), name: 'Scan result');
    });

}
```

**NB**: Ne pas oublie d'appeler la méthode `dispose` de la classe `BarcodeScannerReader` pour libérer les ressources.


## Additional information

TODO: Tell users more about the package: where to find more information, how to 
contribute to the package, how to file issues, what response they can expect 
from the package authors, and more.
