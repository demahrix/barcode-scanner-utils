import 'dart:async' show StreamController;
import 'package:flutter_libserialport/flutter_libserialport.dart';

enum BlocState {
  pending,
  error,
  data,
  empty
}

class SerialPortSelectorBloc {

  // _State _currentState = _State.pending;

  List<SerialPort> _ports = [];

  final _controller = StreamController<BlocState>();

  Stream<BlocState> get listen => _controller.stream;
  List<SerialPort> get ports => _ports;


  void getPorts() {
    try {

      _controller.sink.add(BlocState.pending);

      if (_ports.isNotEmpty) {
        _closePort(_ports);
        _ports = []; // FIXME librer les ressouces peut-etre mettre tous a `null`
      }

      final address = SerialPort.availablePorts;

      for (int i=0, len=address.length; i<len; ++i) {
        _ports.add(SerialPort(address[i]));
      }

      _controller.sink.add(_ports.isEmpty ? BlocState.empty : BlocState.data);
      
    } catch (e) {
      _controller.sink.add(BlocState.error);
    }
  }

  static void _closePort(List<SerialPort> ports) {
    for (int i=0, len=ports.length; i<len; ++i)
      ports[i].close();
  }

  void dipose() {
    _closePort(_ports);
    _controller.close();
  }

}