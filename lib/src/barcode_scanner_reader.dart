import 'dart:developer';
import 'dart:typed_data' show Uint8List;
import 'package:flutter_libserialport/flutter_libserialport.dart';

class BarcodeScannerReader {

  final SerialPort _port;
  final SerialPortReader _reader;

  BarcodeScannerReader._(this._port, this._reader);

  /// Se connecte au premier scanner dont le nom correspond
  static BarcodeScannerReader? create(List<String> scannerNames) {

    for (final address in SerialPort.availablePorts) {
      final port = SerialPort(address);

      if (scannerNames.contains(port.productName)) {
        log(port.openRead().toString(), name: "is open");
        return BarcodeScannerReader._(port, SerialPortReader(port));
      }

      port.dispose();
    }

    return null;
  }

  /// On envoie tous les address il en choisir une
  // static BarcodeScannerReader? select(String selectCallbal(List<String>)) {

  // }

  Stream<Uint8List> get stream => _reader.stream;

  void dispose() {
    _reader.close();
    _port.dispose();
  }

}
