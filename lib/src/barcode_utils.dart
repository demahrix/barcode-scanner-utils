import 'package:barcode_scanner_utils/src/models/my_barcode_type.dart';
import 'package:barcode_widget/barcode_widget.dart';

class BarcodeUtils {

  static Barcode getBarcodeType(String data) {

    int length = data.length;

    MyBarcodeType type = MyBarcodeType.values.firstWhere((e) {
      List<int> counts = e.length;
      if (counts.length == 1)
        return length == counts.first;
      if (counts.first <= length && length <= counts.last)
        return true;
      return false;
    }, orElse: () => MyBarcodeType.CODABAR);

    switch(type) {
      case MyBarcodeType.EAN2:
        return Barcode.ean2();
      case MyBarcodeType.EAN5:
        return Barcode.ean5();
      case MyBarcodeType.EAN8:
        return Barcode.ean8(drawSpacers: true);
      case MyBarcodeType.EAN13:
        return Barcode.ean13(drawEndChar: true);
      case MyBarcodeType.UPC_A:
        return Barcode.upcA();
      case MyBarcodeType.UPC_E:
        return Barcode.upcE();
      case MyBarcodeType.ITF_14:
        return Barcode.itf14();
      case MyBarcodeType.ITF_16:
        return Barcode.itf16();
    }

    return Barcode.codabar(); // or Barcode.code128();
  }

}
