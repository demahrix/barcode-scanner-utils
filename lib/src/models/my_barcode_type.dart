
class MyBarcodeType {

  /*
      Les barcodes qu'on peut dessiner (source: https://pub.dev/packages/barcode)
        Specification: https://davbfr.github.io/dart_barcode/#/
      - Code 39
      - Code 93
      - Code 128 A
      - Code 128 B
      - Code 128 C
      - GS1-128
      - Interleaved 2 of 5 (ITF)
      - ITF-14
      - ITF-16
      - EAN 13
      - EAN 8
      - EAN 2
      - EAN 5
      - ISBN
      - UPC-A
      - UPC-E
      - Telepen
      - Codabar
      - RM4SCC
      - QR-Code
      - PDF417
      - Data Matrix
      - Aztec
    */

  // Ignored
  // Telepen
  // RM4SCC

  final List<int> length; // <int>[length] ou <int>[min length, max length]
  final bool isNumeric;

  const MyBarcodeType(this.length, this.isNumeric);

  static const EAN2 = MyBarcodeType([2], true);

  static const EAN5 = MyBarcodeType([5], true);

  static const EAN8 = MyBarcodeType([7, 8], true);

  static const EAN13 = MyBarcodeType([12, 13], true);

  static const ISBN = MyBarcodeType([12, 13], true);

  static const UPC_A = MyBarcodeType([11, 13], true);

  static const UPC_E = MyBarcodeType([6, 13], true);

  static const ITF_14 = MyBarcodeType([13, 14], true);

  // FIXME has check function
  static const ITF_16 = MyBarcodeType([15, 16], true);

  // Default
  static const CODABAR = MyBarcodeType([1, 1000], false);

  static List<MyBarcodeType> get values => [
    EAN2,
    EAN5,
    EAN8,
    EAN13,
    ISBN,
    UPC_A,
    UPC_E,
    ITF_14,
    ITF_16,
    CODABAR
  ];

}
