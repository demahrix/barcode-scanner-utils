import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';

// FIXME rename 
class BarcodeKeyboardScanner extends StatefulWidget {

  final bool autofocus;
  final ValueChanged<String> onScan;
  // final Widget waitingWidget;
  // final Widget 

  const BarcodeKeyboardScanner({
    Key? key,
    this.autofocus = true,
    required this.onScan
  }): super(key: key);

  @override
  State<BarcodeKeyboardScanner> createState() => _BarcodeKeyboardScannerState();
}

/// Le code scanner doit etre terminer par le caractere CARRIAGE RETURNS (entrer)
class _BarcodeKeyboardScannerState extends State<BarcodeKeyboardScanner> {

  // static const int SEQUENCE_MAX_TIME = 20;
  final FocusNode _focusNode = FocusNode();
  final StringBuffer _stringBuffer = StringBuffer();
  // int? _lastTypedTime;

  @override
  void initState() {
    super.initState();
    if (widget.autofocus)
      _focusNode.requestFocus();
    _focusNode.addListener(() {
      if (mounted)
        setState(() {});
    });
  }

  void _onKey(RawKeyEvent e) {
    if (e is! RawKeyDownEvent)
      return;

    String? char = e.character;

    if (char == null) {
      if (e.logicalKey == LogicalKeyboardKey.enter && _stringBuffer.isNotEmpty)
        widget.onScan(_stringBuffer.toString());
      _stringBuffer.clear();
      return;
    }

    _stringBuffer.write(char);

    // if (_lastTypedTime != null) {
    //   log((time - _lastTypedTime!).toString(), name: 'Time');
    // }

    // log(char.toString(), name: 'Character');
    // _lastTypedTime = time;
  }

  @override
  Widget build(BuildContext context) {
    return RawKeyboardListener(
      onKey: _onKey,
      focusNode: _focusNode,
      child: _focusNode.hasFocus
        ? _waitingWidget()
        : _requestFocusWidget()
    );
  }

  Widget _waitingWidget() {
    return Lottie.asset(
      'assets/animations/4089-barcode-scanner.json',
      package: 'barcode_scanner_utils'
    );
  }

  Widget _requestFocusWidget() {
    return TextButton(
      onPressed: _focusNode.requestFocus,
      child: const Row(
        children: [
          Icon(Icons.barcode_reader), // FIXME replace with barcode_scanner when available
          SizedBox(width: 8.0,),
          Text('Cliquer ici pour scanner') // FIXME typo
        ],
      ),
    );
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _stringBuffer.clear();
    super.dispose();
  }

}
