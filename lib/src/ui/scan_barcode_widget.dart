import 'dart:developer';

import 'package:flutter/material.dart';
import '../../barcode_scanner_utils.dart';
import 'barcode_utils_widget.dart';

class ScanBarcodeWidget extends StatefulWidget {

  final List<String> scannersName;
  final double width;
  final double height;
  final int animationDuration;
  final ValueChanged<String>? onChanged;

  const ScanBarcodeWidget({
    Key? key,
    required this.scannersName,
    this.onChanged,
    this.width = 160,
    this.height = 60,
    this.animationDuration = 800
  }) : super(key: key);

  @override
  State<ScanBarcodeWidget> createState() => _ScanBarcodeWidgetState();
}

class _ScanBarcodeWidgetState extends State<ScanBarcodeWidget> {

  BarcodeScannerReader? _barcodeReader;

  String? _currentData;

  @override
  void initState() {
    super.initState(); 
    _init(true);
  }

  void _init([bool firstTime = false]) {
    _barcodeReader = BarcodeScannerReader.create(widget.scannersName);

    if (!firstTime)
      setState(() {});

    if (_barcodeReader != null) {
      _barcodeReader!.stream.listen((rawData) {
        String data = String.fromCharCodes(rawData).trim();

        if (data != _currentData) {
          setState(() { _currentData = data; });
          widget.onChanged?.call(data);
        }

      }, onError: (error) {
        log(error.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      child: _barcodeReader == null
        ? _noScannerWidget()
        : _currentData == null
          ? WaitingScanBarcodeWidget(
              width: widget.width,
              height: widget.height,
              duration: widget.animationDuration,
            )
          : BarcodeUtilsWidget(
              data: _currentData!,
              width: widget.width,
              height: widget.height,
            )
          //Text('data $_currentData')
    );
  }

  Widget _noScannerWidget() {
    return Tooltip(
      message: "Cliquez pour recherhce a nouveau", // FIXME a avec accent
      child: TextButton.icon(
        onPressed: _init,
        icon: const Icon(Icons.cached_outlined), // FIXME trouve le bon icon
        label: const Text("Aucun scanner trouvé")
      ),
    );
  }

  @override
  void dispose() {
    _barcodeReader?.dispose();
    super.dispose();
  }

}
