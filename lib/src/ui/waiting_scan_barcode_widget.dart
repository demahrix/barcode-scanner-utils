import 'dart:ui' show PointMode;

import 'package:flutter/material.dart';


@Deprecated("")
class WaitingScanBarcodeWidget extends StatefulWidget {

  final double width;
  final double height;

  /// Durer de l'animatiom en milliseconds
  final int duration;

  const WaitingScanBarcodeWidget({
    Key? key,
    this.duration = 800,
    this.width = 160,
    this.height = 60
  }): super(key: key);

  @override
  _WaitingScanBarcodeWidgetState createState() => _WaitingScanBarcodeWidgetState();
}

class _WaitingScanBarcodeWidgetState extends State<WaitingScanBarcodeWidget> with SingleTickerProviderStateMixin {

  late final AnimationController _controller;
  late final Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(duration: Duration(milliseconds: widget.duration), vsync: this);
    
    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller)
    ..addListener(() {
      setState(() {});
    })
    ..addStatusListener((status) {
      if (status == AnimationStatus.completed)
        _controller.reverse();
      else if (status == AnimationStatus.dismissed)
        _controller.forward();
    });

    _controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      height: widget.height,
      child: CustomPaint(
        painter: _CustomPainter(_animation.value),
        willChange: true,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

}

double lerp(double a, double b, double t) => a * (1.0 - t) + b * t;

class _CustomPainter extends CustomPainter {

  /// valeur normalise
  final double indicatorPosition;
  final Paint myPaint;

  _CustomPainter(this.indicatorPosition): myPaint = Paint()
      ..style = PaintingStyle.stroke
      ..color = Colors.blue;

  @override
  void paint(Canvas canvas, Size size) {

    final minSize = size.shortestSide / 4;

    final double indicatorHorizontalPad = size.width * 0.05;

    final double indicatorHeight = size.height * lerp(0.06, 0.94, indicatorPosition);

    canvas.drawLine(
      Offset(indicatorHorizontalPad, indicatorHeight),
      Offset(size.width - indicatorHorizontalPad, indicatorHeight),
      myPaint
    );

    // canvas.drawShadow(Path(), Colors.blue, 8.0, true);

    // En haut a droite
    canvas.drawPoints(
      PointMode.lines,
      [
        Offset(minSize, 0),
        const Offset(0, 0),
        const Offset(0, 0),
        Offset(0, minSize)
      ],
      myPaint
    );

    // En haut a gauche
    canvas.drawPoints(
      PointMode.lines,
      [
        Offset(size.width - minSize, 0),
        Offset(size.width, 0),
        Offset(size.width, 0),
        Offset(size.width, minSize)
      ],
      myPaint
    );

    // En bas a droite
    canvas.drawPoints(
      PointMode.lines,
      [
        Offset(size.width - minSize, size.height),
        Offset(size.width, size.height),
        Offset(size.width, size.height),
        Offset(size.width, size.height - minSize)
      ],
      myPaint
    );

    // En bas a gauche
    canvas.drawPoints(
      PointMode.lines,
      [
        Offset(0, size.height - minSize),
        Offset(0, size.height),
        Offset(0, size.height),
        Offset(minSize, size.height)
      ],
      myPaint
    );

  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => indicatorPosition != (oldDelegate as _CustomPainter).indicatorPosition;

}
