import 'package:flutter/material.dart';
import 'package:flutter_libserialport/flutter_libserialport.dart' show SerialPort;
import 'package:barcode_scanner_utils/src/serial_port_selector/serial_port_selector_bloc.dart';

// FIXME verifier qu'il est correcte
class SerialPortSelectorWidget extends StatefulWidget {

  final ValueSetter<SerialPort> onChoose;

  const SerialPortSelectorWidget({
    Key? key,
    required this.onChoose
  }) : super(key: key);

  @override
  State<SerialPortSelectorWidget> createState() => _SerialPortSelectorWidgetState();
}

class _SerialPortSelectorWidgetState extends State<SerialPortSelectorWidget> {

  final _bloc = SerialPortSelectorBloc();

  @override
  void initState() {
    super.initState();
    _bloc.getPorts();
  }

  void _onTap(SerialPort port) {
    widget.onChoose(SerialPort.fromAddress(port.address));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BlocState>(
      stream: _bloc.listen,
      builder: (_, snapshot) {
        final state = snapshot.data;
        if (state == BlocState.empty)
          return _emptyWidget;
        else if (state == BlocState.error)
          return _errorWidget;
        if (state == BlocState.data)
          return _dataWidget;
        return const Center(child: CircularProgressIndicator());
      }
    );
  }

  @override
  void dispose() {
    _bloc.dipose();
    super.dispose();
  }

  Widget get _dataWidget {
    // mettre un widget qui scroll
    final ports = _bloc.ports;
    return Column(
      children: List.generate(
        ports.length,
        (index) {
          final port = ports[index];
          return ListTile(
            onTap: () => _onTap(port),
            title: Text(port.productName ?? port.name ?? 'N/A'),
            subtitle: Text('Adresse: ${port.macAddress ?? port.address}'),
          );
        }
      ),
    );
  }

  static Widget get _emptyWidget {
    return const Text('Aucun port');
  }

  static Widget get _errorWidget => const Text(
    "Une erreur s'est produite",
    style: TextStyle(
      color: Colors.red
    ),
  );

}
