import 'package:barcode_scanner_utils/barcode_scanner_utils.dart';

class BarcodeUtilsWidget extends BarcodeWidget {

  BarcodeUtilsWidget({
    super.key,
    required String data,
    super.width,
    super.height,
    super.drawText,
    super.backgroundColor,
    super.color,
    super.decoration,
    super.errorBuilder,
    super.margin,
    super.padding,
    super.style,
    super.textPadding
  }): super(
    data: data,
    barcode: BarcodeUtils.getBarcodeType(data)
  );

}
