import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:barcode_scanner_utils/barcode_scanner_utils.dart';
import 'package:barcode_scanner_utils/src/_new/barcode_scanner_mode.dart';
import 'package:flutter/widgets.dart' show FocusNode;

class BarcodeScannerProvider {

  final BarcodeScannerMode mode;
  final FocusNode? _focusNode;

  final SerialPort? _serialPort;
  SerialPortReader? _reader;

  StreamSubscription<Uint8List>? _streamSubscription;

  final StreamController<String> _controller = StreamController();

  BarcodeScannerProvider._({ required this.mode, FocusNode? focusNode, SerialPort? serialPort })
    : _focusNode = focusNode, _serialPort = serialPort {
      _init();
    }

  factory BarcodeScannerProvider.fromKeyboard() {
    return BarcodeScannerProvider._(mode: BarcodeScannerMode.keyboard, focusNode: FocusNode());
  }

  /// Throw exception if no port with specified `vendorId` and `productId` not found \
  /// Exception: `Exception("port not found")` 
  factory BarcodeScannerProvider.fromUsb(int vendorId, int productId) {
    for (var name in SerialPort.availablePorts) {
      var port = SerialPort(name);
      if (port.vendorId == vendorId && port.productId == productId)
        return BarcodeScannerProvider._(mode: BarcodeScannerMode.usb, serialPort: port);
      // FIXME close port ???
      // port.dispose();
    }

    throw Exception("port not found");
  }

  factory BarcodeScannerProvider.fromBluetooth() {
    throw UnimplementedError();
  }

  Stream<String> get stream => _controller.stream;
  FocusNode? get focusNode => _focusNode;

  void _init() {
    switch(mode) {
      case BarcodeScannerMode.keyboard:
        _initFromKeyboard();
        break;
      case BarcodeScannerMode.usb:
        _initFromUsb();
        break;
      case BarcodeScannerMode.bluetooth:
        throw UnimplementedError();
    }
  }

  void _initFromKeyboard() {
    // Use isolate ???
    // Isolate.spawn((message) { }, message)
  }

  void _initFromUsb() {
    var port = _serialPort!;
    if (!port.isOpen)
      port.open(mode: SerialPortMode.read);
    _reader = SerialPortReader(port);

    _streamSubscription = _reader!.stream.listen((event) {
      // FIXME use ascii ? or ut8 ? or latin1 ?
      _controller.sink.add(ascii.decode(event));
    });
  }

  void dispose() {
    _controller.close();
    _focusNode?.dispose();
    _streamSubscription?.cancel();
    _reader?.close();
  }

}
